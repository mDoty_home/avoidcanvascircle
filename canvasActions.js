// JavaScript source code
var canvas;
var context;
var canvasPos;

//items related to the mouse object.
var mouseData = {
    x: 0,
    y: 0
}

//Items related to the circle object.
var circleData = {
    circleGo: true,
    circleX: 90,
    circleY: 90,
    radius: 40,
    colour_go: '#27e833',
    strokeStyle_go: '#003300',
    colour_stop: '#fb122f',
    strokeStyle_stop: '#8b0000',
    borderWidth: 5,
    directionAngle: Math.PI / 6,
    speed: 1
}

function WithinBorderLimit(dx, dy) {
    //Validate that circle appears within border of canvas.
    if ((circleData.circleX + dx >= circleData.radius + circleData.borderWidth / 2) && //Left Limit
        (circleData.circleX + dx < canvas.width - circleData.radius - circleData.borderWidth / 2) && //Right Limit
        (circleData.circleY + dy < canvas.height - circleData.radius - circleData.borderWidth / 2) && //Bottom Limit
         (circleData.circleY + dy >= circleData.radius + circleData.borderWidth / 2)  //Top Limit
    )
        return true;
    else {
        console.log("Outside Border Limit.")
        return false;
    }
}

function UncaughtPointer(dx, dy) {
    //Pythag therom to determine if pointer exists outside circle.
    var fullCircleRadius = circleData.radius + circleData.borderWidth / 2;
    var legA = Math.abs(circleData.circleX + dx - mouseData.x);
    var legB = Math.abs(circleData.circleY + dy - mouseData.y);

    var legC = Math.sqrt(Math.pow(legA, 2) + Math.pow(legB, 2));
    if (legC > fullCircleRadius) {
        return true;
    } else {
        console.log("Caught Pointer:" + legA + " " + legB + " " + legC);
        return false;
    }
}

function PointerInsideCanvas() {
    //Determine if pointer remins inside the canvas.
    var borderWidth = context.lineWidth;

    if ((mouseData.x > borderWidth) //Left Limit
        && (mouseData.x < canvas.width)  //Right Limit
         && (mouseData.y < canvas.height) //Bottom Limit
        && (mouseData.y > borderWidth)  //Top Limit
    )
        return true;
    else {
        console.log("Pointer Outside:" + mouseData.x + " " + mouseData.y + " " + borderWidth);
        return false;
    }
}

function ValidatePosition(dx, dy) {
    //Validate to determine if game rules have been met.
    return WithinBorderLimit(dx, dy) && UncaughtPointer(dx, dy) && PointerInsideCanvas()
}

function UpdateCircle() {
    //Update circle data.
    var dx = circleData.speed * Math.cos(circleData.directionAngle);
    var dy = circleData.speed * Math.sin(circleData.directionAngle);

    if (ValidatePosition(dx, dy)) {
        //Update circle direction.
        circleData.circleGo = true;
        circleData.circleX += dx;
        circleData.circleY += dy;

        DrawCircle();
    } else {
        circleData.circleGo = false;
        CircleStatus();
    }

}

function CircleStatus() {
    if (circleData.circleGo) {
        circleData.speed += 0.01;
        context.fillStyle = circleData.colour_go;
        context.fill();

        context.strokeStyle = circleData.strokeStyle_go;
        context.stroke();
    } else {
        circleData.speed = 1;

        context.fillStyle = circleData.colour_stop;
        context.fill();

        context.strokeStyle = circleData.strokeStyle_stop;
        context.stroke();
    }
}

function DrawCircle() {

    //Draw the circle within the canvas.
    context.clearRect(0, 0, canvas.width, canvas.height);

    context.beginPath();
    context.arc(circleData.circleX, circleData.circleY, circleData.radius, 0, 2 * Math.PI, false);
    CircleStatus();

    context.lineWidth = circleData.borderWidth;

}

function GetMouseData(offset, e) {
    //Return the current position of the mouse.
    return {
        x: e.pageX - offset.left,
        y: e.pageY - offset.top
    }
}

function SetCircleDirection(e) {
    //Set new circle direction based off mouse position.
    var offset = $(this).offset();
    mouseData = GetMouseData(offset, e);
    console.log("Mouse Position: " + mouseData.x + " " + mouseData.y);
    circleData.directionAngle = Math.atan2(mouseData.y - circleData.circleY, mouseData.x - circleData.circleX);
}

function InitFunctions() {
    //Initial setup.
    canvas = document.getElementById('myCanvas');
    context = canvas.getContext('2d');

    setInterval(UpdateCircle, 20);

    canvas.addEventListener("mousemove", SetCircleDirection, false);
    console.log(event.target);
}

